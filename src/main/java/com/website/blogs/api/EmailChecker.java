package com.website.blogs.api;

import org.springframework.stereotype.Component;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.model.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;

import java.io.IOException;
import java.util.List;

@Component
public class EmailChecker {
    /*
     * В этом проекте реализована система верификации через email
     * и соответсвенно нужен сервис для проверки email, чтобы пользователь
     * вводил корректный email. Тут мы используем сторонее API для проверки email-а
     */

    private static final String API_KEY = "dcgfzvrR3fBvdAZrpeZSS";

    // Учетные данные для доступа к вашей учетной записи AWS
    private static final String ACCESS_KEY = "AKIATCKARC3E6KMVQVJK";
    private static final String SECRET_KEY = "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";


    // Создание клиента Amazon SES
    private static final AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY)))
            .withRegion(Regions.EU_NORTH_1)
            .build();


    public boolean checkEmail(String email) {

        // Заранее инициализиурем переменную, где будем хранить ответ
        String response = "";

        // URL для проверки адреса электронной почты, взятый из API
        String url =
                "https://apps.emaillistverify.com/api/verifEmail?secret=" + API_KEY + "&email=" + email;

        // Создаем HTTP-клиент
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

            // Создаем GET-запрос
            HttpGet request = new HttpGet(url);

            // Отправляем запрос и получаем ответ
            response = httpClient.execute(request, httpResponse -> {
                // Печатаем тело ответа
                return EntityUtils.toString(httpResponse.getEntity());

            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        /* При проверке почты, API может выдавать очень много ошибок:
         *  Например: Invalid Syntax, Domain not found, Mailbox full
         * А правильный ответ только 1 - ok, поэтому просто сверяем ответ с ok
         */

        return response.equals("ok");
    }

    public void authenticate(String email) {
        // Добавление email-адреса в список идентификаторов Amazon SES
        addEmailIdentity(client, email);
    }

    // Метод для добавления email-адреса в список идентификаторов Amazon SES
    private static void addEmailIdentity(AmazonSimpleEmailService client, String emailAddress) {
        VerifyEmailIdentityRequest request = new VerifyEmailIdentityRequest().withEmailAddress(emailAddress);
        VerifyEmailIdentityResult result = client.verifyEmailIdentity(request);
    }

    // Метод для проверки почты из identities amazon SES, чтобы проверить, подтвердил ли пользователь почту
    public boolean checkEmailInIdentity(String email) {

        // Формирование запроса на получение списка идентификаторов Amazon SES
        ListIdentitiesRequest request = new ListIdentitiesRequest().withIdentityType(IdentityType.EmailAddress);

        // Получение списка идентификаторов
        ListIdentitiesResult result = client.listIdentities(request);

        // Проверка, содержится ли email в списке идентификаторов
        List<String> identities = result.getIdentities();

        return identities.contains(email);
    }
}
